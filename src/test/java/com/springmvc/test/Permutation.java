package com.springmvc.test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yongsheng.he
 * @describe
 * @date 2017/12/21 15:20
 */
public class Permutation
{

    public static List<String> permutation = new ArrayList<String>();

    public static void main(String[] args)
    {
        char[] numbers = new char[]{'1', '2', '3', '4'};
        permute(numbers, 0, numbers.length - 1);

        for (String str : permutation)
        {
            System.out.println(str);
        }
    }


    private static void permute(char[] n, int cur, int end)
    {
        if (cur == end)
        {
            String str = new String(n);
            if (permutation.contains(str) || str.charAt(2) == '4' || str.contains("12") || str.contains("21"))
                return;
            permutation.add(str);
            return;
        }
        for (int i = cur; i <= end; i++)
        {
            swap(n, cur, i);
            permute(n, cur + 1, end);
            swap(n, cur, i);
        }
    }

    private static void swap(char[] n, int cur, int i)
    {
        char temp = n[cur];
        n[cur] = n[i];
        n[i] = temp;
    }
}