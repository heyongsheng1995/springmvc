package com.springmvc.test;


import org.junit.Test;

/**
 * @author yongsheng.he
 * @describe
 * @date 2017/12/21 15:28
 */
public class Tests
{
    public static void main(String[] args)
    {

        for (int i = 1000; i < 9999; i++)
        {
            String str = String.valueOf(i);
            if (str.charAt(2) == '4' || str.contains("12") || str.contains("21"))
                continue;
            System.out.println(str);
        }
    }

    @Test
    public void test()
    {
        int iValue = 6;
        String binaryString = Integer.toBinaryString(iValue);
        System.out.println(binaryString);
    }
}
