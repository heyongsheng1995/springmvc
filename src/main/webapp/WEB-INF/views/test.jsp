<%--
  User: yongsheng.he
  Date: 2018/1/11
  Time: 20:52
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>测试</title>
</head>
<body>
<ui id="aaa">
    <li value="1">aaa</li>
    <li value="2">bbb</li>
    <li value="3">ccc</li>
    <li value="4">ddd</li>
    <li value="5">eee</li>
    <li value="6">fff</li>
</ui>
<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.js"></script>
<script>
    $('#aaa').find('li').each(function () {
        console.info("value-"+$(this).val());
        console.info($(this).index())
    })
</script>
</body>
</html>