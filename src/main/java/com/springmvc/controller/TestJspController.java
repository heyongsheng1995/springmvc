package com.springmvc.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author yongsheng.he
 * @describe 测试jsp
 * @date 2018/01/11 20:03
 */
@Controller
@Slf4j
public class TestJspController
{

    @GetMapping("/test")
    public String test(HttpServletRequest request, ModelMap modelMap)
    {
        System.out.println(111);
        System.out.println(222);
        System.out.println(333);
        System.out.println(444);

        return "test";
    }
}
