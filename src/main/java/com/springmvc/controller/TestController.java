package com.springmvc.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.springmvc.common.ErrorInfo;
import com.springmvc.models.RequestModel;
import com.springmvc.models.ResponseModel;
import com.springmvc.pojo.Student;
import com.springmvc.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author yongsheng.he
 * @describe
 * @date 2017/12/21 13:53
 */
@RestController
@Slf4j
public class TestController
{
    @Autowired
    private StudentService studentService;

    @PostMapping("/save")
    public ResponseModel<Student> save(@RequestParam Integer i)
    {
        for (int j = 0; j < i; j++)
        {
            Student student = new Student();
            student.setName("name" + (j + 1));
            student.setAge(j + 1);
            student.setCreateTime(new Date());
            RequestModel<Student> requestModel = new RequestModel<Student>(student);

            studentService.insert(student);
        }
        return new ResponseModel<Student>(ErrorInfo.ERROR_CODE_SUCCESS.getErrCode(), ErrorInfo.ERROR_CODE_SUCCESS.getErrName());
    }

    @GetMapping("/pagingQuery")
    public ResponseModel<PageInfo> pagingQuery(@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize, @RequestParam(value = "pageNo", defaultValue = "0") Integer pageNo)
    {
        PageHelper.startPage(pageNo, pageSize);
        PageInfo<Student> pageInfo = new PageInfo<>(studentService.queryAll());

        return new ResponseModel<>(ErrorInfo.ERROR_CODE_SUCCESS.getErrCode(), ErrorInfo.ERROR_CODE_SUCCESS.getErrName(), pageInfo);
    }

}
