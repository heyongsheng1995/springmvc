package com.springmvc.common;

public enum ErrorInfo
{
    /**
     * 前两位:系统or模块 00 后四位:具体详细错误
     */
    ERROR_CODE_SUCCESS("000000", "成功"),

    ERROR_CODE_ERROR("999999", "系统异常"),

    REQUEST_METHOD_ERROR("999991", "接口请求方法错误"),

    REQUEST_PARAMETER_IS_INCORRECT("999992", "请求参数有误");

    private String errCode;
    private String errName;

     ErrorInfo(String errCode, String errName)
    {
        this.errCode = errCode;
        this.errName = errName;
    }

    public String getErrName()
    {
        return errName;
    }

    public void setErrName(String errName)
    {
        this.errName = errName;
    }

    public String getErrCode()
    {
        return errCode;
    }

    public void setErrCode(String errCode)
    {
        this.errCode = errCode;
    }

}
