package com.springmvc.mapper;

import com.github.abel533.mapper.Mapper;
import com.springmvc.pojo.Student;

public interface StudentMapper extends Mapper<Student> {
}