package com.springmvc.service.impl;

import com.springmvc.mapper.StudentMapper;
import com.springmvc.pojo.Student;
import com.springmvc.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author yongsheng.he
 * @describe
 * @date 2017/12/21 10:58
 */
@Service
@Slf4j
public class StudentServiceImpl implements StudentService
{

    @Autowired
    private StudentMapper studentMapper;

    @Override
    public Integer insert(Student student)
    {
        return studentMapper.insert(student);
    }

    @Override
    public List<Student> queryAll()
    {
        return studentMapper.select(null);
    }
}
