package com.springmvc.service;

import com.springmvc.pojo.Student;

import java.util.List;

/**
 * @author yongsheng.he
 * @describe
 * @date 2017/12/21 10:57
 */
public interface StudentService
{
    Integer insert(Student student);

    List<Student> queryAll();
}
