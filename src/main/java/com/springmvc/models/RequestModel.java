package com.springmvc.models;

import lombok.*;

import java.io.Serializable;

/**
 * @author yongsheng.he
 * @describe 请求模型
 * @date 2017/12/22 11:24
 */
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
public class RequestModel<T> implements Serializable
{
    private static final long serialVersionUID = -2748750238220288302L;
    private String code;
    @NonNull
    private T data;
}
