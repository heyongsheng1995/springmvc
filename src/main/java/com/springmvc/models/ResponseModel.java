package com.springmvc.models;

import lombok.*;

import java.io.Serializable;

/**
 * @author yongsheng.he
 * @describe 响应模型
 * @date 2017/12/22 10:45
 */
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
public class ResponseModel<T> implements Serializable
{
    private static final long serialVersionUID = -5652798121857492829L;
    @NonNull
    private String code;
    @NonNull
    private String msg;
    private T data;
}
