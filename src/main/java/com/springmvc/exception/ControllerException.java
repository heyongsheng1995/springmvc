package com.springmvc.exception;

/**
 * 自定义异常
 */
public class ControllerException extends Exception
{

    /**
     * 返回的错误代码
     */
    private String code;

    /**
     * 错误消息
     */
    private String message;

    /**
     * 错误消息参数列表
     */
    private String[] args;


    public ControllerException(String code, Exception e)
    {
        super(e);
        this.code = code;
    }

    public ControllerException(String code, String message, Object[] args)
    {
        this.code = code;
        this.message = String.format(message, args);
    }

    public ControllerException(String code, String message)
    {
        this.code = code;
        this.message = message;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    @Override
    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String[] getArgs()
    {
        return args;
    }

    public void setArgs(String[] args)
    {
        this.args = args;
    }
}